--Write a script that deletes the database hbtn_0c_0 in your MySQL server.
--If the database hbtn_0c_0 doesn’t exist, your script should not fail

drop database if exists hbtn_0c_0;