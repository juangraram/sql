/*Import in hbtn_0c_0 database this table dump: download (same as Temperatures #0)
Write a script that displays the max temperature of each state (ordered by State name).*/

select state, max(value) as max_temp from temperatures group by state order by state asc;
