/*Import in hbtn_0c_0 database this table dump: download
Write a script that displays the average temperature (Fahrenheit) by city ordered by temperature (descending).*/

--mysql -uUSERNAME -p DB_NAME < import_file.sql
select city, avg(value) as avg_temp from temperatures group by city order by avg_temp desc;
