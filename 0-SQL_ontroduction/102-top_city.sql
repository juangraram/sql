/*Import in hbtn_0c_0 database this table dump: download (same as Temperatures #0)
Write a script that displays the top 3 of cities temperature during July and August ordered by temperature (descending)*/

select city, avg(value) as avg_temp from temperatures where month = 7 or month = 8 group by city order by avg_temp desc limit 3;
