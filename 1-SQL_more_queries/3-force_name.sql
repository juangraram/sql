/*Write a script that creates the table force_name on your MySQL server.
force_name description: id INT name VARCHAR(256) can’t be null
If the table force_name already exists, your script should not fail*/

create table if not exists force_name (id INT, name VARCHAR(256) not null);
ALTER DATABASE hbtn_0d_2 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE hbtn_0d_2.force_name convert TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
