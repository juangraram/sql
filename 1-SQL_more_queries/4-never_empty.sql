/*Write a script that creates the table id_not_null on your MySQL server.
id_not_null description: id INT with the default value 1 name VARCHAR(256)
If the table id_not_null already exists, your script should not fail*/

create table if not exists id_not_null (id INT default 1, name VARCHAR(256)) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
INSERT INTO id_not_null (name) VALUES ("Holberton");
INSERT INTO id_not_null (id, name) VALUES (89, "Holberton School");

/*Write a script that creates the table unique_id on your MySQL server.
unique_id description: id INT with the default value 1 and must be unique name VARCHAR(256)
If the table unique_id already exists, your script should not fail*/

create table if not exists unique_id (id INT default 1 unique, name VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci);
INSERT INTO unique_id (id, name) VALUES (89, "Holberton School");

/*Write a script that creates the database hbtn_0d_usa and the table states (in the database hbtn_0d_usa) on your MySQL server.
states description: id INT unique, auto generated, can’t be null and is a primary key name VARCHAR(256) can’t be null
If the database hbtn_0d_usa already exists, your script should not fail
If the table states already exists, your script should not fail*/

create database if not exists hbtn_0d_usa CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
create table if not exists states (id INT unique auto_increment not null primary key, name VARCHAR(256) not null);
INSERT INTO states (name) VALUES ("California"), ("Arizona"), ("Texas");
INSERT INTO states (name) VALUES ("Utah");

/*Write a script that creates the database hbtn_0d_usa and the table cities (in the database hbtn_0d_usa) on your MySQL server.
cities description: id INT unique, auto generated, can’t be null and is a primary key 
state_id INT, can’t be null and must be a FOREIGN KEY that references to id of the states table
name VARCHAR(256) can’t be null
If the database hbtn_0d_usa already exists, your script should not fail
If the table cities already exists, your script should not fail*/

create table if not exists cities (id INT unique auto_increment not null primary key, state_id int not null, foreign key (state_id) references states(id), name VARCHAR(256) not null);
INSERT INTO cities (state_id, name) VALUES (1, "San Francisco");
INSERT INTO cities (state_id, name) VALUES (1, "San Jose");
INSERT INTO cities (state_id, name) VALUES (4, "Page");
INSERT INTO cities (state_id, name) VALUES (3, "Paris");
INSERT INTO cities (state_id, name) VALUES (3, "Houston");
INSERT INTO cities (state_id, name) VALUES (3, "Dallas");
